import mongoose, { Schema } from "mongoose";

const studentSchema = new Schema({
  firstName: String,
  lastName: String,
  age: Number,
});

export const Student = mongoose.model("Student", studentSchema);