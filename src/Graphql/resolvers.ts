import { Student } from "../Models/student";

// GraphQL Resolvers
const resolvers = {
  Query: {
    hello: () => "Hello from Reflectoring Blog",
    welcome: (parent: any, args: any) => `Hello ${args.name}`,
    students: async () => await Student.find({}), // return array of students
    student: async (parent: any, args: any) => await Student.findById(args.id), // return student by id
  },
  Mutation: {
    create: async (parent: any, args: any) => {
      const { firstName, lastName, age } = args;
      const newStudent = await Student.create({
        firstName,
        lastName,
        age,
      })

      return newStudent;
    },
    update: async (parent: any, args: any) => {
      const { id } = args;
      const updatedStudent = await Student.findByIdAndUpdate(id, args);
      if (!updatedStudent) {
        throw new Error(`Student with ID ${id} not found`);
      }
      return updatedStudent;
    },
    delete: async (parent: any, args: any) => {
      const { id } = args;
      const deletedStudent = await Student.findByIdAndDelete(id);
      if (!deletedStudent) {
        throw new Error(`Student with ID ${id} not found`);
      }
      return deletedStudent;
    },
  },
};
  
export { resolvers };