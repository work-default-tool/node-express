import { Router } from "express";
import { PostController } from '../Controllers/post.controller'

const postRoute = () => {
  const router = Router();

  router.post("/", PostController.addpost);

  router.get("/", PostController.getPosts);

  router.get("/:id", PostController.getAPost);

  router.put("/:id", PostController.updatePost);

  router.delete("/:id", PostController.deletePost);

  return router;
}
  
export { postRoute };