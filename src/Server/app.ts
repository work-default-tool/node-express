import express from 'express'
import { db } from '../Config/db.config'
import { postRoute } from '../Routes/posts.routes'

// import { ApolloServer } from '@apollo/server';
// import { startStandaloneServer } from "@apollo/server/standalone";
// import { resolvers } from "../Graphql/resolvers";
// import { typeDefs } from "../Graphql/typeDefs";

const app = express()

//middlewares
app.use(express.json())
app.use(express.urlencoded({extended: true}))

//routes
app.use('/api/v1/posts', postRoute())

//db connection then server connection
db.then(() => {
    app.listen(7070, () => console.log('Server is listening on port 7070'))
})


// start graphql applo server
// const server = new ApolloServer({ typeDefs, resolvers });

// db.then(() => {
//     startStandaloneServer(server, {
//       listen: { port: 4000 },
//     }).then(({ url }) => {
//       console.log(`Server ready at ${url}`);
//     });
// })
